package mainframe;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class BtnExecuteListener implements ActionListener {

	MainFrame mainFrame;
	public BtnExecuteListener(MainFrame mainFrame) {
		this.mainFrame = mainFrame;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		mainFrame.listingElements();
		try {
			mainFrame.setDate();
		} catch(Exception ex) {
			ex.printStackTrace();
		}
	}

}
