package mainframe;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import jgp.std.JGPIO;

public class MainFrame extends JFrame {
	private static final long serialVersionUID = 2562272456057180248L;
	
	private JCheckBox checkCompareExtFile;
	private JCheckBox recursionListingDestFolders;
	private JCheckBox recursionListingSrcFolders;
	private JCheckBox folderModification;
	
	File[] destElements;
	File[] sourceElements;

	ArrayList<File> destElementsList;
	ArrayList<File> sourceElementsList;

	public MainFrame(String title) {
		super(title);

		JPanel panel = new JPanel();
		panel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		
		// btnPanel
		JPanel btnPanel = new JPanel();
		btnPanel.setAlignmentX(JComponent.CENTER_ALIGNMENT);
		btnPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		btnPanel.setLayout(new BoxLayout(btnPanel, BoxLayout.X_AXIS));
		
		JButton btnSrcElements = new JButton("Elementy �rod�owe");
		btnSrcElements.addActionListener(new BtnSrcElementsListener(this));

		JButton btnDestElements = new JButton("Elementy docelowe");
		btnDestElements.addActionListener(new BtnDestElementsListener(this));

		btnPanel.add(btnSrcElements);
		btnPanel.add(Box.createRigidArea(new Dimension(5,0)));
		btnPanel.add(btnDestElements);
		
		// checkPanel
		JPanel checkPanel = new JPanel();
		checkPanel.setAlignmentX(JComponent.CENTER_ALIGNMENT);
		checkPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		checkPanel.setLayout(new BoxLayout(checkPanel, BoxLayout.Y_AXIS));
		
		checkCompareExtFile = new JCheckBox("Por�wnuj rozszerzenia plik�w");
		recursionListingSrcFolders = new JCheckBox("Listuj rekurencyjnie foldery �r�d�owe");
		recursionListingDestFolders = new JCheckBox("Listuj rekurencyjnie foldery docelowe");
		folderModification = new JCheckBox("Modyfikuj foldery");
		
		checkPanel.add(checkCompareExtFile);
		checkPanel.add(recursionListingSrcFolders);
		checkPanel.add(recursionListingDestFolders);
		checkPanel.add(folderModification);
		
		// executePanel
		JPanel executePanel = new JPanel();
		executePanel.setAlignmentX(JComponent.CENTER_ALIGNMENT);
		executePanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		executePanel.setLayout(new BoxLayout(executePanel, BoxLayout.Y_AXIS));
		
		JButton btnExecute = new JButton("Wykonaj");
		btnExecute.addActionListener(new BtnExecuteListener(this));
		executePanel.add(btnExecute);
		
		panel.add(btnPanel);
		panel.add(checkPanel);
		panel.add(executePanel);

		this.getContentPane().add(BorderLayout.CENTER, panel);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize(480, 530);
		this.setVisible(true);
	}

	/*
	 * Listowanie
	 */
	
	public void listingElements() {
		listingSrcElements();
		listingDestElements();
    }

	public void listingDestElements() {
		destElementsList = new ArrayList<File>();
		for(File f: destElements) {
			if(f.isFile())
				destElementsList.add(f);
			else if(f.isDirectory())
				listAndAddFolder(f, destElementsList, recursionListingDestFolders.isSelected());
		}
	}
	
	public void listingSrcElements() {
    	sourceElementsList = new ArrayList<File>();
		for(File f: sourceElements) {
			if(f.isFile())
				sourceElementsList.add(f);
			else if(f.isDirectory())
				listAndAddFolder(f, sourceElementsList, recursionListingSrcFolders.isSelected());
		}
	}

	private void listAndAddFolder(File f, ArrayList<File> elementsList, boolean isRecursion) {
		File dir = new File(f.getPath());
		File listedDir[] = dir.listFiles();
		for(int i = 0; i < listedDir.length; i++) {
			if(folderModification.isSelected() == true || listedDir[i].isFile())
				elementsList.add(listedDir[i]);
			if(isRecursion && listedDir[i].isDirectory())
				listAndAddFolder(listedDir[i], elementsList, true);
		}
	}
	
	/*
	 * end Listowanie
	 */
    
    public void setDate() throws IOException {
    	int goodChangeCounter = 0;
    	for(int i = 0; i < sourceElementsList.size(); i++) {
    		for(int j = 0; j < destElementsList.size(); j++) {
				if(checkCompareExtFile.isSelected()) {
					if(destElementsList.get(j).getName().equals(sourceElementsList.get(i).getName()) == true &&
						destElementsList.get(j).lastModified() != sourceElementsList.get(i).lastModified() &&
						(JGPIO.equalFile(sourceElementsList.get(i), destElementsList.get(j)) || JGPIO.equalDirectory(sourceElementsList.get(i), destElementsList.get(j))))
					{
						destElementsList.get(j).setLastModified(sourceElementsList.get(i).lastModified());
						destElementsList.remove(j);
						goodChangeCounter++;
						break;
					}
				}
				else {
					int sourceLastIndex = sourceElementsList.get(i).getName().lastIndexOf(".");
					int destLastIndex = destElementsList.get(j).getName().lastIndexOf(".");
					String destFileName = destElementsList.get(j).getName();
					String sourceFileName = sourceElementsList.get(i).getName();
					if(sourceLastIndex != -1)
						sourceFileName = sourceElementsList.get(i).getName().substring(0, sourceLastIndex);
					if(destLastIndex != -1)
						destFileName = destElementsList.get(j).getName().substring(0, destLastIndex);
					
					if(destFileName.equals(sourceFileName) == true &&
						destElementsList.get(j).lastModified() != sourceElementsList.get(i).lastModified() &&
						(JGPIO.equalFile(sourceElementsList.get(i), destElementsList.get(j)) || JGPIO.equalDirectory(sourceElementsList.get(i), destElementsList.get(j))))
					{
						destElementsList.get(j).setLastModified(sourceElementsList.get(i).lastModified());
						destElementsList.remove(j);
						goodChangeCounter++;
						break;
					}
				}
    		}
    	}
    	JOptionPane.showMessageDialog(null, "Liczba przetworzonych element�w: " + goodChangeCounter, "Information", JOptionPane.INFORMATION_MESSAGE);
	}
}
